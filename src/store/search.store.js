import axios from "axios"
import {observable} from "mobx"

const store = observable({
  api_key: '1hZyt0g9X2zWDNbv8sTt6M1ECS6REuwt', //I would've store it in some securestore if it was not for the test purposes
  gifs: [],
  refresh: false,
  loading: false,
  notFound: '', //Didn't want to pass props from searchscreen to searchbar, so using this to display the query if not found
  async searchGifs(q) {
    try {
      this.loading = true
      this.notFound = ''
      const {data: {data: images}} = await axios.get('https://api.giphy.com/v1/gifs/search', {
        params: {api_key: this.api_key, q, limit: 20}
      })
      this.gifs = []
      if (!images.length) {
        this.refresh = !this.refresh
        return this.notFound = q
      }
      for (const el of images) {
        if (el.images.fixed_width.url) {
          this.gifs.push({
            link: el.images.fixed_width_downsampled.url,
            detailedImageLink: el.images.original.url,
            user: el.user
          })
        }
      }
      if (this.gifs.length % 2 === 1) {
        this.gifs.pop()
      }
    } catch (error) {
      console.error(error)
    } finally {
      this.loading = false
      this.refresh = !this.refresh
    }
  }
})

export default store