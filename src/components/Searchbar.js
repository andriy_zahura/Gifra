
import debounce from 'lodash.debounce'
import {observer, useLocalObservable} from 'mobx-react-lite'
import React, {useCallback, useEffect} from 'react'
import {SearchBar} from 'react-native-elements'
import SearchIcon from '../assets/icons/Search'
import searchStore from '../store/search.store'

export default observer(() => {
  const store = useLocalObservable(() => ({
    value: '',
    async debouncedFn() {
      await searchStore.searchGifs(store.value)
    }
  }))

  const getSearchResults = useCallback(debounce(store.debouncedFn, 200), [])

  useEffect(() => {
    if (store.value !== '') {
      getSearchResults()
    } else {
      searchStore.gifs = []
    }
  }, [store.value])

  return (
    <SearchBar
      placeholder="Search GIPHY"
      cancelButtonProps={{
        buttonStyle: {
          height: 56,
          width: 80,
          borderRadius: 8,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff',
          marginLeft: 8,
        },
        buttonTextStyle: {
          fontWeight: '700',
          fontSize: 15,
          lineHeight: 20,
          textAlign: 'center'
        },
        color: 'rgba(17, 24, 39, 0.87)',
      }}
      containerStyle={{
        backgroundColor: '#000',
      }}
      inputContainerStyle={{
        height: 56,
        backgroundColor: '#17181A',
        borderWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, 0.12)',
        borderRadius: 16,
      }}
      inputStyle={{
        color: '#fff',
      }}
      placeholderTextColor="rgba(255, 255, 255, 0.6)"
      searchIcon={SearchIcon}
      onChangeText={(val) => store.value = val}
      value={store.value}
      onCancel={() => {
        store.value = ''
        searchStore.notFound = ''
        searchStore.gifs = []
        searchStore.refresh = !searchStore.refresh
      }}
      platform="ios"
    />
  )
})