import {CardStyleInterpolators, createStackNavigator} from '@react-navigation/stack'
import {observer} from 'mobx-react-lite'
import React from 'react'
import {Platform} from 'react-native'
import Details from '../screens/Details'
import Search from '../screens/Search'

const {Navigator, Screen} = createStackNavigator()

export default observer(() => {
  return (
    <>
      <Navigator initialRouteName="search">
        <Screen
          name="search"
          component={Search}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="details"
          component={Details}
          options={{
            headerShown: false,
            cardStyleInterpolator: Platform.OS === 'ios' ?
              CardStyleInterpolators.forHorizontalIOS :
              CardStyleInterpolators.forRevealFromBottomAndroid
          }}
        />
      </Navigator>
    </>
  )
})
