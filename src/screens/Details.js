
import {StatusBar} from 'expo-status-bar'
import {observer, useLocalObservable} from 'mobx-react-lite'
import React from 'react'
import {Dimensions, FlatList, Image, SafeAreaView, StyleSheet, Text, View} from 'react-native'
import BackIcon from '../assets/icons/Back'
import Gif from '../components/Gif'
import searchStore from '../store/search.store'

const GIF_WIDTH = Dimensions.get('window').width * 0.98

const DetailsHeader = observer(({gif}) => {
  return (
    <View>
      <BackIcon style={styles.backIcon} />
      <View style={styles.mainGifContainer}>
        <Image
          source={{uri: gif.detailedImageLink}}
          alt="gif"
          resizeMode="cover"
          style={styles.mainGif}
        />
      </View>
      <View style={styles.profileContainer}>
        <Image
          source={{uri: gif.user?.avatar_url}}
          style={styles.profileImage}
        />
        <View style={styles.profileNames}>
          <Text style={styles.profileHeader}>{gif.user?.display_name}</Text>
          <Text style={styles.profileFooter}>@{gif.user?.username}</Text>
        </View>
      </View>
      <Text style={styles.text}>Related GIFs</Text>
    </View>
  )
})

export default observer(({route}) => {
  const store = useLocalObservable(() => ({
    gif: route.params.gif
  }))
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="light" />
      <FlatList
        ListHeaderComponent={() => {
          if (store.gif) {
            return (<DetailsHeader gif={store.gif} />)
          }
        }}
        contentContainerStyle={styles.flatlistContainer}
        data={searchStore.gifs}
        numColumns={2}
        columnWrapperStyle={{
          justifyContent: 'space-between'
        }}
        renderItem={({item: gif}) => {
          if (store.gif === gif) {return null} // I thought it's faster to compare gif items to the current gif than to filter through array
          return (
            <Gif gif={gif} />
          )
        }}
        keyExtractor={(gif, index) => index.toString()}
      >
      </FlatList>
    </SafeAreaView>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: '#000',
    paddingLeft: 8,
    paddingRight: 8,
    zIndex: 0,
  },
  backIcon: {
    position: 'relative',
    top: 60,
    left: 8,
    zIndex: 1,
  },
  mainGif: {
    width: GIF_WIDTH,
    height: GIF_WIDTH,
  },
  mainGifContainer: {
    overflow: 'hidden',
    borderRadius: 24,
  },
  profileContainer: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 16,
    justifyContent: 'flex-start'
  },
  profileImage: {
    width: 48,
    height: 48,
    borderRadius: 24,
    borderWidth: 1,
  },
  profileNames: {
    marginLeft: 8
  },
  profileHeader: {
    fontSize: 18,
    fontFamily: 'Inter_400Regular',
    color: '#fff',
  },
  profileFooter: {
    fontFamily: 'Inter_400Regular',
    fontSize: 12,
    color: '#fff',
  },
  flatlistContainer: {
    width: '100%',
    flexGrow: 1,
    justifyContent: !searchStore.gifs.length ? 'space-between' : 'center',
    alignItems: 'stretch',
  },
  text: {
    fontSize: 17,
    fontFamily: 'Inter_700Bold',
    color: '#FFFFFFDE',
    letterSpacing: -0.41,
  }
})