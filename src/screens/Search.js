
import {StatusBar} from 'expo-status-bar'
import {observer, useLocalObservable} from 'mobx-react-lite'
import React, {useEffect} from 'react'
import {Dimensions, FlatList, SafeAreaView, StyleSheet, Text, View} from 'react-native'
import AnimatedLoader from "react-native-animated-loader"
import Gif from '../components/Gif'
import Searchbar from '../components/Searchbar'
import searchStore from '../store/search.store'

const PHONE_HEIGHT = (Dimensions.get('window').height)

export default observer(() => {
  const store = useLocalObservable(() => ({
    gifs: [],
    refreshing: false
  }))

  useEffect(() => {
    if (store.gifs !== searchStore.gifs) {
      store.gifs = searchStore.gifs
    }
  }, [searchStore.gifs])

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="light" />
      <Searchbar />
      <FlatList
        style={styles.flatlist}
        contentContainerStyle={styles.flatlistContainer}
        data={store.gifs}
        numColumns={2}
        columnWrapperStyle={{
          justifyContent: 'space-between'
        }}
        renderItem={({item: gif}) => {
          return (
            <Gif gif={gif} />
          )
        }}
        refreshing={searchStore.loading}
        extraData={searchStore.refresh}
        keyExtractor={(gif, index) => index.toString()}
        ListEmptyComponent={() => {
          if (searchStore.loading) {
            return (
              <AnimatedLoader
                visible={true}
                overlayColor="rgba(255,255,255,0)"
                source={require("../assets/spinner.json")}
                animationStyle={styles.lottie}
                speed={1}
              />
            )
          } else if (searchStore.notFound !== '') {
            return (
              <View style={styles.placeholderContainer}>
                <Text style={styles.placeholderText}>No results for {searchStore.notFound}</Text>
              </View>
            )
          }
          return null
        }}
      >
      </FlatList>
    </SafeAreaView >
  )
})

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    backgroundColor: '#000',
    height: PHONE_HEIGHT,
  },
  flatlist: {
    height: '100%',
    marginLeft: 8,
    marginRight: 8
  },
  flatlistContainer: {
    width: '100%',
    flexGrow: 1,
    justifyContent: !searchStore.gifs.length ? 'space-between' : 'center',
    alignItems: 'stretch',
  },
  placeholderContainer: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  placeholderText: {
    color: '#fff',
    fontFamily: 'Inter_700Bold'
  },
  lottie: {
    width: 80,
    height: 80,
  }
})
