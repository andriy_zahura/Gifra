import {Inter_400Regular, Inter_700Bold, useFonts} from '@expo-google-fonts/inter'
import {NavigationContainer} from '@react-navigation/native'
import AppLoading from 'expo-app-loading'
import {configure} from 'mobx'
import React from 'react'
import Navigation from './src/navigation'
import {navigationRef} from './src/navigation/RootNavigation'

configure({enforceActions: false})

export default function App() {
  let [appReady] = useFonts({
    Inter_400Regular, Inter_700Bold
  })

  return (
    <NavigationContainer ref={navigationRef}>
      {!appReady && (
        <AppLoading />
      )}
      {appReady && (
        <Navigation />
      )}
    </NavigationContainer>
  )
}
